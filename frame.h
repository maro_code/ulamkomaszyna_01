#ifndef MYFRAME_H
#define MYFRAME_H

#include <QApplication>
#include <QPushButton>
#include <QWidget>
#include <QObject>
#include <QString>
#include <QFrame>
#include <QTableView>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QDial>
#include <QLCDNumber>
#include <QPushButton>
#include <iostream>
#include <sstream>

//class QFrame;

//class QVBoxLayout;

class MyFrame : public QWidget
{
  Q_OBJECT

 public:

    MyFrame(QVBoxLayout *l, QWidget *parent = 0);

	int f_Compl() const;
	int f_Denom() const;
	int f_Integr() const;
	int f_res_upper() const;
	int f_res_lower() const;
	int f_decimal_checked() const;
	int f_if_checked() const;
	int f_if_po() const;
	int f_if_io() const;


 public slots:

	void set_Compl(int value);
	void set_Denom(int value);
	void set_Integr(int value);
	void set_if_checked(int value);
	void set_res_upper(int value);
	void set_res_lower(int value);
	void send_to_terminal();
	void set_decimal_checked(int value);
	void set_po(int value);
	void set_io(int value);

 signals:

	void Compl_Changed(int newValue);
	void Denom_Changed(int newValue);
	void Integr_Changed(int newValue);
	void if_checked_signal(int newState);
	void res_upper_changed(int newValue);
	void res_lower_changed(int newValue);
	void decimal_checked_signal(int newState);
	void po_signal(int newState);
	void io_signal(int newState);

 private:

	int Complication;
	int Max_Denominator;
	int Max_Integrity;
	int if_checked; //if example appears
	int decimal; //if displays decimal fractions 0 - no, 1 - sometimes, 2 - always if possible
	int Res_Upper;
	int Res_Lower;
	int if_po;
	int if_io;

};

#endif
