#include "fractions.h"

//============================================================================
//============================================================================

//================       SOME ARITHMETICAL FUNCTIONS       ===================

//============================================================================
//============================================================================

//tu najwiekszy wspolny dzielnik
long int gcd(long int a,long int b){
	long int w = 1;

	if (a < 0) a = -a;
	if (b < 0) b = -b;

	if (a == 0 || b == 0){
		if (a == b) return -1;
		if (a > b) return a;
		else return b;
	}

	while (a > 0){
		w = a;
		a = b % a;
		b = w;
	}
	return w;
}

//tu najmniejsza wspolna wielokrotnosc
//(jesli sa ujemne to beda traktowane jak dodatnie)
long int lcm(long int a,long int b){
	long int n;

	n = gcd(a, b);

	return (a * b) / n;
}

long int my_abs(long int x){
	if (x >= 0) return x;
	else return -x;
}

//funkcja zwraca wspolczynnik przez ktory trzeba pomnozyc licznik, zeby mianownik dopelnil sie do potegi 10, jesli liczba nie nadaje sie do zamiany na ulamek dziesietny o skonczonym okresie, albo nie ma czesci ulamkowej zwraca 0
long int decimal_parameter(long int num, long int den){
	long int ptm, pfm;
	int i;

	if (num == 0) return 0;

	ptm = pfm = 0;

	while(((den % 2) == 0) && (den > 0)){
		++ptm;
		den /= 2;
	}

	while(((den % 5) == 0) && (den > 0)){
		++pfm;
		den /= 5;
	}

	if (den == 1){
		for(i = 0; i < my_abs(ptm - pfm); ++i){
			if (ptm > pfm) den *= 5;
			else den *= 2;
		}
	}
	else return 0;

  return den;
}

//tu losuje sie ulamek - licznik zawsze mniejszy od mianownika;
//nigdy nie wychodzi zero;ta wersja skraca licznik i mianownik,
//gdy licznik jest zerowy mianownik ustawiany jest na jeden
//mianownik nigdy nie osiaga gornej granicy
//przekazujemy parametry, czy chcemy miec tylko liczby dodatnie
//i czy tylko calkowite
int random_fraction(struct fraction_s *fr_p, long int max_d,
				long int max_i, int plus_only, int ints_only){

	long int t;

	//ustalamy kwestie znaku
	if (!plus_only){
		if (rand() % 2 == 0)
			fr_p->sign_f = '+';
		else fr_p->sign_f = '-';
	}
	else fr_p->sign_f = '+';

	//ustalamy mianownik
	if(!ints_only){
		t = ((rand() % max_d) + 1);
	}
	else {
		t = 1;
		max_d = 1;
	}
	fr_p->denominator_f = t;

	fr_p->numerator_f = (rand() % t);

	if (max_d > 1) fr_p->integrity_f = (rand() % (max_i + 1));
	else {
		if (max_i > 0) fr_p->integrity_f = ((rand() % max_i) + 1);
		else fr_p->integrity_f = 1;
	}

	if ((fr_p->numerator_f == 0) && (fr_p->integrity_f == 0)){
		fr_p->numerator_f = 1; //po prostu 1/2
		fr_p->denominator_f = 2;
	}

	if (fr_p->numerator_f > 0){
		if ((t = gcd(fr_p->numerator_f,fr_p->denominator_f)) > 1){
			fr_p->numerator_f = fr_p->numerator_f / t;
			fr_p->denominator_f = fr_p->denominator_f / t;
		}
	}
	else fr_p->denominator_f = 1;

	return 0;
}



//tu funkcja losujaca dzialanie
int random_operator(struct fraction_s *fr_p){

	int i;

	fr_p->integrity_f = 0;
	fr_p->numerator_f = 0;
	fr_p->denominator_f = 0;
	i = (rand() % 4);

	switch (i){
		case 0:
			fr_p->sign_f = '+';
        	break;
		case 1:
			fr_p->sign_f = '-';
			break;
		case 2:
			fr_p->sign_f = '*';
			break;
		case 3:
			fr_p->sign_f = '/';
			break;
		default:
			fr_p->sign_f = '+';
			break;
	}

	return 0;
}


//tu funkcja tworzaca jeden przyklad o zadanej zlozonosci
//(tablica,ile ulamkow,jaki zakres mianownika, jaki zakres calosci,
//czy tylko dodatnie, czy tylko calkowite)
int random_example(struct fraction_s example_elements[],
					int all_example_length, long int denominator,
					long int integrity, int plus_only, int ints_only){

	int all_counter, i, fr_counter, op_counter;

	op_counter = 0;

	random_fraction(&example_elements[0], denominator, integrity, plus_only, ints_only);
	random_fraction(&example_elements[1], denominator, integrity, plus_only, ints_only);

	fr_counter = all_counter = 2;

	while (fr_counter < ((all_example_length + 1) / 2)){
		i = (rand() % 2);
		if (op_counter < fr_counter - 1){
			if (i == 0){
				random_fraction(&example_elements[all_counter], denominator, integrity, plus_only, ints_only);
				++fr_counter;
			}
			else{
                random_operator(&example_elements[all_counter]);
                ++op_counter;
			}
		}
		else{
			random_fraction(&example_elements[all_counter], denominator, integrity, plus_only, ints_only);
			++fr_counter;
		}
		++all_counter;
	}

	while (all_counter < all_example_length){
		random_operator(&example_elements[all_counter]);
		++op_counter;
		++all_counter;
	}

	return 0;
}

//funkcja obliczajaca wartosc przykladu ile to dlugosc przykladu liczona razem z dzialaniami *a to zrodlo
int compute_example(struct fraction_s *src_arr, struct fraction_s *stack_p,
					struct fraction_s *res, int all_elements, int p_i_mask){
	int control, i;

	for(i = 0; i < all_elements; ++i){
		//fs_print(src_arr);
		if(src_arr->denominator_f != 0){
			*stack_p = *src_arr;
			++stack_p;
		}
		else{
			switch(src_arr->sign_f){
				case '+':
					control = fs_add(stack_p - 2, stack_p - 1);
					if ((control | p_i_mask) != 3) return FALSE_D;
					break;
				case '-':
					control = fs_subtract(stack_p - 2, stack_p - 1);
					if ((control | p_i_mask) != 3) return FALSE_D;
					break;
				case '*':
					control = fs_multiply(stack_p - 2, stack_p - 1);
					if ((control | p_i_mask) != 3) return FALSE_D;
					break;
				case '/':
					control = fs_divide(stack_p - 2, stack_p - 1);
					if ((control | p_i_mask) != 3) return FALSE_D;
					break;
			}
		--stack_p;
		}
	++src_arr;
	}
	*res = *(stack_p - 1);
	return TRUE_D;
}


//funkcja dodajaca struktury fraction_s wynik zapisywany jest w *fr_p_1
int fs_add(struct fraction_s *fr_p_1, struct fraction_s *fr_p_2){
	long int d, help_1, help_2;
	int dg, to_return;

	//printf("Adding");

	if ((dg = (fs_check(fr_p_1))) != OK_D) return dg;
	if ((dg = (fs_check(fr_p_2))) != OK_D) return dg;

	fs_simplify(fr_p_1);
	fs_simplify(fr_p_2);

	//puts("ADDING - FIRST SIMPLIFY:");
	//fs_print(fr_p_1);
	//fs_print(fr_p_2);

	if ((dg = (fs_check(fr_p_1))) != OK_D) return dg;
	if ((dg = (fs_check(fr_p_2))) != OK_D) return dg;

	//common denominator
	d = lcm(fr_p_1->denominator_f, fr_p_2->denominator_f);

	fr_p_1->numerator_f *= d;
	fr_p_1->numerator_f /= fr_p_1->denominator_f;

	fr_p_2->numerator_f *= d;
	fr_p_2->numerator_f /= fr_p_2->denominator_f;

	fr_p_1->denominator_f = d;
	fr_p_2->denominator_f = d;

	if ((dg = (fs_check(fr_p_1))) != OK_D) return dg;
	if ((dg = (fs_check(fr_p_2))) != OK_D) return dg;

	//puts("ADDING - COMMON DENOMINATOR:");
	//fs_print(fr_p_1);
	//fs_print(fr_p_2);

	//not-proper fraction
	help_1 = fr_p_1->integrity_f * fr_p_1->denominator_f;
	help_2 = fr_p_2->integrity_f * fr_p_2->denominator_f;

	if (help_1 > MAX_NUMBER || help_2 > MAX_NUMBER) return COMPUTE_ERROR;

	fr_p_1->numerator_f += help_1;
	fr_p_2->numerator_f += help_2;

	if ((dg = (fs_check(fr_p_1))) != OK_D) return dg;
	if ((dg = (fs_check(fr_p_2))) != OK_D) return dg;

	fr_p_1->integrity_f = 0;
	fr_p_2->integrity_f = 0;

	//puts("ADDING - NOT-PROPER FRACTIONS:");
	//fs_print(fr_p_1);
	//fs_print(fr_p_2);

	if (fr_p_1->sign_f == '-') fr_p_1->numerator_f = -(fr_p_1->numerator_f);
	if (fr_p_2->sign_f == '-') fr_p_2->numerator_f = -(fr_p_2->numerator_f);

	//adding
	fr_p_1->numerator_f += fr_p_2->numerator_f;

	//puts("ADDING - FIRST RESULT:");
	//fs_print(fr_p_1);
	//fs_print(fr_p_2);


	//change sign if needed
	if (fr_p_1->numerator_f < 0){
		fr_p_1->sign_f = '-';
		fr_p_1->numerator_f = -fr_p_1->numerator_f;
	}
	else fr_p_1->sign_f = '+';

	if ((dg = (fs_check(fr_p_1))) != OK_D) return dg;
	if ((dg = (fs_check(fr_p_2))) != OK_D) return dg;

	fs_simplify(fr_p_1);

	//puts("ADDING - SECOND SIMPLIFY:");
	//fs_print(fr_p_1);

	if (fr_p_1->sign_f == '+'){
		if (fr_p_1->numerator_f != 0) to_return = P_NI;
		else to_return = P_I;
	}
	else{
		if (fr_p_1->numerator_f != 0) to_return = NP_NI;
		else to_return = NP_I;
	}
	//printf(" returns %i\n", to_return);

	return to_return;
}


//funkcja odejmujaca struktury fraction_s wynik zapisywany jest w *wyr1 odjemnik to *wyr2
int fs_subtract(struct fraction_s *fr_p_1, struct fraction_s *fr_p_2){

	//printf("Subtracting");
	if (fr_p_2->sign_f == '-') fr_p_2->sign_f = '+';
	else fr_p_2->sign_f = '-';
	return fs_add(fr_p_1, fr_p_2);
}



//funkcja mnozaca struktury fraction_s wynik zapisywany jest w *wyr1
int fs_multiply(struct fraction_s *fr_p_1, struct fraction_s *fr_p_2){
	long int d, help_1, help_2;
	int dg, to_return;

	//printf("Multiplying");

	if (fr_p_1->numerator_f == 0 && fr_p_1->integrity_f == 1) return COMPUTE_ERROR;
	if (fr_p_2->numerator_f == 0 && fr_p_2->integrity_f == 1) return COMPUTE_ERROR;

	if ((dg = (fs_check(fr_p_1))) != OK_D) return dg;
	if ((dg = (fs_check(fr_p_2))) != OK_D) return dg;

	fs_simplify(fr_p_1);
	fs_simplify(fr_p_2);

	//puts("MULITPLYING - FIRST SIMPLIFY:");
	//fs_print(fr_p_1);
	//fs_print(fr_p_2);

	if ((dg = (fs_check(fr_p_1))) != OK_D) return dg;
	if ((dg = (fs_check(fr_p_2))) != OK_D) return dg;

	//result sign
	if (fr_p_1->sign_f == fr_p_2->sign_f) fr_p_1->sign_f = '+';
	else fr_p_1->sign_f = '-';

	//not-proper fraction
	help_1 = fr_p_1->integrity_f * fr_p_1->denominator_f;
	help_2 = fr_p_2->integrity_f * fr_p_2->denominator_f;

	if (help_1 > MAX_NUMBER || help_2 > MAX_NUMBER) return COMPUTE_ERROR;

	fr_p_1->numerator_f += help_1;
	fr_p_2->numerator_f += help_2;

	if ((dg = (fs_check(fr_p_1))) != OK_D) return dg;
	if ((dg = (fs_check(fr_p_2))) != OK_D) return dg;

	fr_p_1->integrity_f = 0;
	fr_p_2->integrity_f = 0;

	//"cross" simplify
	d = gcd(fr_p_1->numerator_f, fr_p_2->denominator_f);
	fr_p_1->numerator_f /= d;
	fr_p_2->denominator_f /= d;

	d = gcd(fr_p_2->numerator_f, fr_p_1->denominator_f);
	fr_p_2->numerator_f /= d;
	fr_p_1->denominator_f /= d;

	//multiplying
	fr_p_1->numerator_f *= fr_p_2->numerator_f;
	fr_p_1->denominator_f *= fr_p_2->denominator_f;

	if (fr_p_1->numerator_f > MAX_NUMBER || fr_p_2->numerator_f > MAX_NUMBER) return COMPUTE_ERROR;

	fs_simplify(fr_p_1);

	if (fr_p_1->sign_f == '+'){
		if (fr_p_1->numerator_f != 0) to_return = P_NI;
		else to_return = P_I;
	}
	else{
		if (fr_p_1->numerator_f != 0) to_return = NP_NI;
		else to_return = NP_I;
	}
	//printf(" returns %i\n", to_return);

	return to_return;
}


//funkcja dzielaca struktury fraction_s wynik zapisywany jest w *wyr1 dzielnik to *wyr2
int fs_divide(struct fraction_s *fr_p_1, struct fraction_s *fr_p_2){
	long int d, help_1, help_2;
	int dg, to_return;

	//printf("Dividing");

	if (fr_p_2->numerator_f == 0 && fr_p_2->integrity_f == 0) return COMPUTE_ERROR;
	if (fr_p_2->numerator_f == 0 && fr_p_2->integrity_f == 1) return COMPUTE_ERROR;

	if ((dg = (fs_check(fr_p_1))) != OK_D) return dg;
	if ((dg = (fs_check(fr_p_2))) != OK_D) return dg;

	fs_simplify(fr_p_1);
	fs_simplify(fr_p_2);

	if ((dg = (fs_check(fr_p_1))) != OK_D) return dg;
	if ((dg = (fs_check(fr_p_2))) != OK_D) return dg;

	//result sign
	if (fr_p_1->sign_f == fr_p_2->sign_f) fr_p_1->sign_f = '+';
	else fr_p_1->sign_f = '-';

	//not-proper fraction
	help_1 = fr_p_1->integrity_f * fr_p_1->denominator_f;
	help_2 = fr_p_2->integrity_f * fr_p_2->denominator_f;

	if (help_1 > MAX_NUMBER || help_2 > MAX_NUMBER) return COMPUTE_ERROR;

	fr_p_1->numerator_f += help_1;
	fr_p_2->numerator_f += help_2;

	if ((dg = (fs_check(fr_p_1))) != OK_D) return dg;
	if ((dg = (fs_check(fr_p_2))) != OK_D) return dg;

	fr_p_1->integrity_f = 0;
	fr_p_2->integrity_f = 0;

	//revert the second
	d = fr_p_2->numerator_f;
	fr_p_2->numerator_f = fr_p_2->denominator_f;
	fr_p_2->denominator_f = d;

	//"cross" simplify
	d = gcd(fr_p_1->numerator_f, fr_p_2->denominator_f);
	fr_p_1->numerator_f /= d;
	fr_p_2->denominator_f /= d;

	d = gcd(fr_p_2->numerator_f, fr_p_1->denominator_f);
	fr_p_2->numerator_f /= d;
	fr_p_1->denominator_f /= d;

	//multiplying
	fr_p_1->numerator_f *= fr_p_2->numerator_f;
	fr_p_1->denominator_f *= fr_p_2->denominator_f;

	if (fr_p_1->numerator_f > MAX_NUMBER || fr_p_2->numerator_f > MAX_NUMBER) return COMPUTE_ERROR;

	fs_simplify(fr_p_1);

	if (fr_p_1->sign_f == '+'){
		if (fr_p_1->numerator_f != 0) to_return = P_NI;
		else to_return = P_I;
	}
	else{
		if (fr_p_1->numerator_f != 0) to_return = NP_NI;
		else to_return = NP_I;
	}
	//printf(" returns %i\n", to_return);

	return to_return;
}

//simplify fraction
void fs_simplify(struct fraction_s *fr_p){
	long int d;

	d = gcd(fr_p->numerator_f, fr_p->denominator_f);

	fr_p->numerator_f /= d;
	fr_p->denominator_f /= d;

	if (fr_p->numerator_f >= fr_p->denominator_f){
		d = fr_p->numerator_f / fr_p->denominator_f;
		fr_p->numerator_f %= fr_p->denominator_f;
		fr_p->integrity_f += d;
	}

	return;
}

void fs_print(struct fraction_s *fr_p){
	printf("Sign: %c\n", fr_p->sign_f);
	printf("Integrity: %li\n", fr_p->integrity_f);
	printf("Numerator: %li\n", fr_p->numerator_f);
	printf("Denominator: %li\n\n", fr_p->denominator_f);

	return;
}

//check fraction
int fs_check(struct fraction_s *fr_p){
	if (my_abs(fr_p->numerator_f > MAX_NUMBER)) return COMPUTE_ERROR;
	if (my_abs(fr_p->denominator_f > MAX_NUMBER)) return COMPUTE_ERROR;
	if (my_abs(fr_p->integrity_f > MAX_NUMBER)) return COMPUTE_ERROR;
	if (my_abs(fr_p->denominator_f == 0)) return COMPUTE_ERROR;

	return OK_D;
}

//zamienia liczbe calkowita na ciag znakow
//UWAGA !!!
//NIE MA KONTROLI ROZMIARU TABLICY DOCELOWEJ

void my_itoa(char *w,long int li_argument){
	long int i;
	char sign_of_arg, tmp_arr[MAX_LENGTH];

	i = 0;
	if (li_argument < 0){
		sign_of_arg = '-';
		li_argument = - li_argument;
	}
	else sign_of_arg = '+';

	do{
	tmp_arr[i] = ((li_argument % 10) + '0');
	li_argument = li_argument / 10;
	++i;
	} while (li_argument > 0);

	if (sign_of_arg == '-'){
		tmp_arr[i] = sign_of_arg;
		++i;
	}

	for(i = i - 1; i >= 0; --i){
		*w = tmp_arr[i];
		++w;
	}
	*w = '\0';
}


//funkcja przerabiajaca ulamek w zapisie struktury fraction_s na napis,
//niezerowy parametr a wlacza obsluge ulamkow dziesietnych

void frs_to_mss(struct fraction_s *fr_p, struct my_string_s *ms_p,long int if_decimal_notation){

	long int w,dl;
	char help[MAX_LENGTH];
	char * ms_arr_p = ms_p->ms_s_characters_arr;

	//diagnostics
	if(fr_p->denominator_f == 0){
		printf("Operator: %c", fr_p->sign_f);
		strcpy(ms_arr_p, "\0");

		return;
	}
	strcpy(ms_arr_p, "\0");
	if (fr_p->sign_f == '-') strcat(ms_arr_p, "(-");

	if (fr_p->integrity_f > 0){
		my_itoa(help, fr_p->integrity_f);
		strcat(ms_arr_p, help);
	}

	w = 0;
	if (if_decimal_notation){
		if ((w = decimal_parameter(fr_p->numerator_f, fr_p->denominator_f)) > 0){
			if (fr_p->integrity_f == 0) strcat(ms_arr_p, "0");
			strcat(ms_arr_p, ",");
			dl = fr_p->numerator_f * w;
			while ((dl % 10) == 0) dl = dl / 10;
				//here help array is rewrited
				my_itoa(help,dl);
			strcat(ms_arr_p,help);
		}
	}

	if (w == 0){
		if (fr_p->numerator_f > 0){
			//why space before \\frac ?
			strcat(ms_arr_p,"\\frac{");
			my_itoa(help, fr_p->numerator_f);
			strcat(ms_arr_p,help);
			strcat(ms_arr_p,"}{");
			my_itoa(help,fr_p->denominator_f);
			strcat(ms_arr_p,help);
			strcat(ms_arr_p,"}");
		}
	}
	if (fr_p->sign_f == '-') strcat(ms_arr_p,")");

	return;
}

//to TeX, ile to dlugosc przykladu(liczone razem z dzialaniami)
struct my_string_s  *format(struct fraction_s *fr_p, struct my_string_s *stack_p,
							 int all_elements, long int dec_not, int if_colon){
	int i, op_counter, l;
	int op_number = ((all_elements - 1) / 2);
	int if_parens;

	if (dec_not > 0) l = 1;
	if (dec_not < 0) l = 0;

	op_counter = 0;

	for(i = 0; i < all_elements; ++i){
		if (dec_not == 0){
			l = (rand() % 2);
		}
		//if fraction
		if(fr_p->denominator_f != 0){
			frs_to_mss(fr_p, stack_p, l);
			++stack_p;
		}
		//if operator
		else{
			if_parens = FALSE_D;
			++op_counter;
			switch(fr_p->sign_f){
				case '+':
					if (op_counter < op_number) if_parens = TRUE_D;
					ms_add(stack_p - 2, stack_p - 1, if_parens);
					break;
				case '-':
					if (op_counter < op_number) if_parens = TRUE_D;
					ms_subtract(stack_p - 2, stack_p - 1, if_parens);
					break;
				case '*':
					ms_multiply(stack_p - 2, stack_p - 1, if_parens);
					break;
				case '/':
					ms_divide(stack_p - 2, stack_p - 1, if_parens, if_colon);
					break;
			}
			--stack_p;
		}
		++fr_p;
     }

	return stack_p - 1;
}

//dodawanie tekstu napisow;
//nawiasow nie dodaje gdy nawiasy=='n',wynik wpisuje do tablicy z pierwszym adresem
void ms_add(struct my_string_s *a, struct my_string_s *b, int if_paren){
	struct my_string_s tmp;
	char * ms_arr_p_tmp = tmp.ms_s_characters_arr;
	char * ms_arr_p_a = a->ms_s_characters_arr;
	char * ms_arr_p_b = b->ms_s_characters_arr;

	strcpy(ms_arr_p_tmp,"\0");
	if (if_paren) strcat(ms_arr_p_tmp, "(");

	strcat(ms_arr_p_tmp, ms_arr_p_a);
	strcat(ms_arr_p_tmp, "+");
	strcat(ms_arr_p_tmp, ms_arr_p_b);

	if (if_paren) strcat(ms_arr_p_tmp, ")");

	*a = tmp;
	return;
}

//odejmowanie tekstu napisow;
//nawiasow nie dodaje gdy nawiasy=='n',wynik wpisuje do tablicy z pierwszym adresem
void ms_subtract(struct my_string_s *a, struct my_string_s *b, int if_paren){
	struct my_string_s tmp;
	char * ms_arr_p_tmp = tmp.ms_s_characters_arr;
	char * ms_arr_p_a = a->ms_s_characters_arr;
	char * ms_arr_p_b = b->ms_s_characters_arr;

	strcpy(ms_arr_p_tmp,"\0");
	if (if_paren) strcat(ms_arr_p_tmp, "(");

	strcat(ms_arr_p_tmp, ms_arr_p_a);
	strcat(ms_arr_p_tmp, "-");
	strcat(ms_arr_p_tmp, ms_arr_p_b);

	if (if_paren) strcat(ms_arr_p_tmp, ")");

	*a = tmp;
	return;
}

//mnozenie tekstu napisow;
//nawiasow nie dodaje gdy nawiasy=='n',wynik wpisuje do tablicy z pierwszym adresem
void ms_multiply(struct my_string_s *a, struct my_string_s *b, int if_paren){
	struct my_string_s tmp;
	char * ms_arr_p_tmp = tmp.ms_s_characters_arr;
	char * ms_arr_p_a = a->ms_s_characters_arr;
	char * ms_arr_p_b = b->ms_s_characters_arr;

	strcpy(ms_arr_p_tmp,"\0");
	if (if_paren) strcat(ms_arr_p_tmp, "(");

	strcat(ms_arr_p_tmp, ms_arr_p_a);
	strcat(ms_arr_p_tmp, "\\cdot");
	strcat(ms_arr_p_tmp, ms_arr_p_b);

	if (if_paren) strcat(ms_arr_p_tmp, ")");

	*a = tmp;
	return;
}

//dzielenie tekstu napisow;
//nawiasow nie dodaje gdy nawiasy=='n',wynik wpisuje do tablicy z pierwszym adresem
void ms_divide(struct my_string_s *a, struct my_string_s *b, int if_paren, int if_colon){
	struct my_string_s tmp;
	char * ms_arr_p_tmp = tmp.ms_s_characters_arr;
	char * ms_arr_p_a = a->ms_s_characters_arr;
	char * ms_arr_p_b = b->ms_s_characters_arr;

	strcpy(ms_arr_p_tmp,"\0");
	if (if_paren) strcat(ms_arr_p_tmp, "(");

	if(!if_colon) strcat(ms_arr_p_tmp, "\\frac{");
	strcat(ms_arr_p_tmp, ms_arr_p_a);
	if(if_colon) strcat(ms_arr_p_tmp, ":");
		else strcat(ms_arr_p_tmp, "}{");
	strcat(ms_arr_p_tmp, ms_arr_p_b);
	if(!if_colon) strcat(ms_arr_p_tmp, "}");

	if (if_paren) strcat(ms_arr_p_tmp, ")");

	*a = tmp;
	return;
}

//if 'begin_spaces' it leaves spaces at the beginning
void cut_spaces(char *a, int begin_spaces){
	char *b;

	if(begin_spaces){
		while(*a == ' ') ++a;
	}
	b = a;
	while(*b != '\0'){
		if (*b != ' '){
			*a = *b;
			++a;
		}
		++b;
	}
	*a = *b;

	return;
}

//remove - from fractions like (-7\frac{3}{4}) if possible
void parse_1(char *a){
	int i = 0;

	++a;
	while (*a != '\0'){
		if (*a == '-' && *(a-1) == '('){
			if (*(a - 2) != '+' && *(a - 2) != '-' && *(a - 2) != 't' && *(a - 2) != ':'){
				i = 0;
				*(a - 1)=' ';
				while (*(a + i) != ')') ++i;
				*(a + i) = ' ';
				a = a + i - 1;
			}
		}
		++a;
	}

	return;
}

//remove () from nominator, and denominator (look out - after multiplying, and dividing)
void parse_2(char *a){
	int i, z, l_curly, r_curly, l_paren, r_paren;

	z = i = 0;
	while (*a != '\0'){
		if (*a == '{' && *(a + 1) == '('){
			z = r_curly = r_paren = 0;
			l_curly = l_paren = 1;
			i = 1;
			while (l_curly - r_curly > 0){
				++i;
				if (*(a + i) == '}') ++r_curly;
				if (*(a + i) == '{') ++l_curly;
				//only multiplication and dividing are written without parens
				//because there is no need, they have priority
				if (*(a + i) == ')') ++r_paren;
				if (*(a + i) == '(') ++l_paren;
				if ((l_paren - r_paren) == 0 && (*(a + i) == 't' || *(a + i) == ':')) z = 1;
			}
			if (z == 0){
				*(a + 1) = ' ';
				*(a + i - 1) = ' ';
			}
			//printf("z value: %i\n", z);
		}
	++a;
	}

	return;
}

//removes parens made through adding
void parse_3(char *a){
	int i, l_paren, r_paren;

	i = 0;
	while (*a != '\0'){
		if (*a == '(' && *(a + 1) != '-'){
			r_paren = 0;
			l_paren = i = 1;
			while (l_paren - r_paren > 0){
				if (*(a + i) == ')') ++r_paren;
				if (*(a + i) == '(') ++l_paren;
				++i;
			}
			if (*(a - 1) != '-' && *(a - 1) != 't' && *(a - 1) != ':' && *(a + i) != '\\'){
				*a = ' ';
				*(a + i - 1) = ' ';
			}
		}
		++a;
	}

	return;
}

//needs spaces before
//?
void parse_4(char *a){
	while (*a != '\0'){
		if ((*a == 't' && *(a + 1) == '(')){
			*a = *(a - 1) = *(a - 2) = *(a - 3) = *(a - 4) = ' ';
		}
		++a;
	}

	return;
}

//removes parens
void parse_res(char *a){

	int i = 0;

	while (*a != '\0'){
		if (*a == '(' || *a == ')'){
			*a = ' ';
		}
		++i;
		++a;
	}
	if (i == 0) strcpy(a, "0");

	return;
}


void parse_all(char *a, int if_dot_rem){
	char tmp[MAX_LENGTH];

	strcpy(tmp, "          ");
	strcat(tmp, a);
	//printf("Start:\n\'%s\'\n\n", tmp);
	cut_spaces(tmp, TRUE_D);
	//printf("After cut_spaces:\n\'%s\'\n\n", tmp);
	parse_1(tmp);
	//printf("After parse_1:\n\'%s\'\n\n", tmp);
	cut_spaces(tmp, TRUE_D);
	//printf("After cut_spaces:\n\'%s\'\n\n", tmp);
	parse_2(tmp);
	//printf("After parse_2:\n\'%s\'\n\n", tmp);
	cut_spaces(tmp, TRUE_D);
	//printf("After cut_spaces:\n\'%s\'\n\n", tmp);
	parse_3(tmp);
	//printf("After parse_3:\n\'%s\'\n\n", tmp);
	if(if_dot_rem){
		cut_spaces(tmp, TRUE_D);
		//printf("After cut_spaces:\n\'%s\'\n\n", tmp);
		parse_4(tmp);
		//printf("After parse_4:\n\'%s\'\n\n", tmp);
	}
	cut_spaces(tmp, FALSE_D);
	//printf("cut_spaces, finish:\n\'%s\'\n\n", tmp);
	strcpy(a, tmp);

	return;
}



//THE MOST IMPORTANT FUNCTION

int execute_work(std::string identyfikator, std::string main_path, MyBar *mybar_data){

	struct fraction_s example_array[MAX_COMPLICATION];
	struct fraction_s fs_stack[MAX_COMPLICATION], fs_result;
	//struct fraction_s *fr_p_1;

	struct my_string_s mss_stack[MAX_COMPLICATION], mss_result_example, mss_result;
	struct my_string_s *mss_p_1;

	char name[MAX_LENGTH],
		path_1[MAX_LENGTH],
		path_2[MAX_LENGTH],
		path_3[MAX_LENGTH],
		path_4[MAX_LENGTH],
		idt[MAX_LENGTH],
		id[MAX_LENGTH],
		file_header[MAX_LENGTH];

	int i, m, j, k, n, compute_signal, complication_arr[100],
		how_many_examples, sheets, how_many_of_all, mask[100],
		try_counter, if_positive_only[100], if_ints_only[100], if_colon_notation,
		if_last_1 = 0, if_last_2 = 0;
	//int f;

	long int cd[100], denominators_arr[100], integritys_arr[100],
		limit_l[100], limit_u[100], h, difficulty;

	FILE *f1, *f2, *f3, *f4;
	time_t t_var;
	MyFrame *frame_pointer;
	srand (time(&t_var));

	sheets = mybar_data->get_how_many();
	how_many_examples = 0;



 //liczymy ile faktycznie bedzie przykladow

	for(i = 0; i < mybar_data->get_examples_counter();++i){
		if ((frame_pointer = (mybar_data->get_example_pointer(i))) && (frame_pointer->f_if_checked() > 0)){
			how_many_examples++;
			complication_arr[how_many_examples - 1] = frame_pointer->f_Compl();
			denominators_arr[how_many_examples - 1] = frame_pointer->f_Denom();
			integritys_arr[how_many_examples - 1] = frame_pointer->f_Integr();

			if_positive_only[how_many_examples - 1] = frame_pointer->f_if_po();
			if_ints_only[how_many_examples - 1] = frame_pointer->f_if_io();
			mask[how_many_examples - 1] = (if_positive_only[how_many_examples - 1] / 2) +  if_ints_only[how_many_examples - 1];


			if (frame_pointer->f_res_upper() > frame_pointer->f_res_lower()){
				limit_u[how_many_examples - 1] = frame_pointer->f_res_upper();
				limit_l[how_many_examples - 1] = frame_pointer->f_res_lower();
			}
			else{
				limit_u[how_many_examples-1] = frame_pointer->f_res_lower();
				limit_l[how_many_examples-1] = frame_pointer->f_res_upper();
			}
		cd[how_many_examples - 1] = ((frame_pointer->f_decimal_checked()) - 1);
		//-1 no; 0 random; 1 yes
		}
		else continue;
    // (mybar_data->get_example_pointer(i))->send_to_terminal();
 	}

	//TO AVOID ERROR
	complication_arr[how_many_examples + 1] = 0;

	if (how_many_examples == 0){
		mybar_data->no_examples();
		return -1;
 	}

	how_many_of_all = sheets * how_many_examples;

	n = 0;
	h = 0;

	const char * help_name = identyfikator.c_str();
	const char * help_path = main_path.c_str();

	strcpy(name, help_name);

	mybar_data->pbar_reset();

	strcpy(path_1, help_path);
	strcpy(path_2, help_path);
	strcpy(path_3, help_path);
	strcpy(path_4, help_path);

	strcat(path_1,"/");
	strcat(path_2,"/");
	strcat(path_3,"/");
	strcat(path_4,"/");

	strcat(path_1,name);
	strcat(path_2,name);
	strcat(path_3,name);
	strcat(path_4,name);

	strcat(path_1,"_b.tex");
	strcat(path_2,"_a.tex");
	strcat(path_3,"_c.tex");
	strcat(path_4,"_d.tex");

	change_id(name);
   
	if (!((f1 = fopen(path_1,"w")) && (f2 = fopen(path_2,"w")) && (f3 = fopen(path_3,"w")) && (f4 = fopen(path_4,"w"))))  {
		std::cout << "dir:" << main_path << std::endl;
		mybar_data->dir_error();
		return -1;
	}

	if_colon_notation = 0;

	/*std::cout << "PO" << if_positive_only << "\nIO" << if_ints_only << "\nCN" << if_colon_notation << "\nMASK" << mask << std::endl;
	fclose(f1);
	fclose(f2);
	fclose(f3);
	return -1;*/

	strcpy(file_header,"\\documentclass[12pt,a4paper,oneside]{article}\n\\usepackage[latin2]{inputenc}\n\\usepackage[english,polish]{babel}\n\\usepackage{polski}\n\\usepackage{tabularx}\n\\setlength{\\textheight}{26cm}\n\\setlength{\\footskip}{-20mm}\n\\setlength{\\hoffset}{-30mm}\n\\setlength{\\voffset}{-4mm}\n\\textwidth = 18cm\n\\setlength{\\topmargin}{-20mm}\n\\setlength{\\headsep}{5mm}\n\\pagestyle{empty}\n\n\\begin{document}\n\n");

	fprintf(f1,"%s",file_header);
	fprintf(f2,"%s",file_header);
	fprintf(f3,"%s\\twocolumn\n\n",file_header);
	fprintf(f4,"%s",file_header);

	print_info(f1, how_many_examples, denominators_arr, integritys_arr,
				limit_l, limit_u, name, mask);

	print_info(f3, how_many_examples, denominators_arr, integritys_arr,
				limit_l, limit_u, name, mask);

//============================================================================
//============================================================================
//================        THE MAIN LOOP - FOR SHEETS       ===================
//============================================================================
//============================================================================

	for(j = 0; j < sheets; ++j){
		++h;

		if_last_1 = 0;
		if_last_2 = 0;

		my_itoa(id,h);
		strcpy(idt, name);
		strcat(idt,"\\ nr\\ ");
		strcat(idt, id);

		fprintf(f1, "\\mbox{\n\\begin{tabularx}{\\textwidth}{XX}\n\\multicolumn{2}{l}{Imi\\k{e} i nazwisko:\\hspace{200pt}Klasa:\\hspace{85pt}%s}\\\\", idt);
		fprintf(f1, "\n\\ & \\\\\n");

		fprintf(f2, "\\mbox{\n\\begin{tabularx}{\\textwidth}{XX}\n\\multicolumn{2}{l}{Imi\\k{e} i nazwisko:\\hspace{200pt}Klasa:\\hspace{85pt}%s}\\\\", idt);
		fprintf(f2, "\n\\ & \\\\\n");

		fprintf(f3, "%s ", idt);

		fprintf(f4, "\\parbox{\\textwidth}{%s ", idt);


//============================================================================
//================        THE LOOP FOR GOOD EXAMPLE        ===================
//============================================================================

		for(k = 0; k < how_many_examples; ++k){
			++n;
			compute_signal = FALSE_D;
			try_counter = 0;

//================          THE LOOP FOR EXAMPLE           ===================

			while (compute_signal == FALSE_D){
				++try_counter;
				//When all approach fails
				if (try_counter > (mybar_data->get_try_bar())){
					fclose(f1);
					fclose(f2);
					fclose(f3);
					fclose(f4);

					f1=fopen(path_1,"w");
					f2=fopen(path_2,"w");
					f3=fopen(path_3,"w");
					f4=fopen(path_4,"w");

					fprintf(f1,"%s",file_header);
					fprintf(f2,"%s",file_header);
					fprintf(f3,"%s",file_header);
					fprintf(f4,"%s",file_header);

					fprintf(f1, "NIE UDA\\L{}O SI\\k{E}\n\\end{document}\n");
					fprintf(f2, "NIE UDA\\L{}O SI\\k{E}\n\\end{document}\n");
					fprintf(f3, "NIE UDA\\L{}O SI\\k{E}\n\\end{document}\n");
					fprintf(f4, "NIE UDA\\L{}O SI\\k{E}\n\\end{document}\n");

					fclose(f1);
					fclose(f2);
					fclose(f3);
					fclose(f4);
 
					std::cout << "bulba!";
					mybar_data->try_over();
					return -1;
				}


				//printf("komplikacja ogolna: %i\n", (complication_arr[k] * 2 - 1));

				random_example(example_array, complication_arr[k] * 2 - 1, denominators_arr[k], integritys_arr[k], if_positive_only[k], if_ints_only[k]);
				//for (f = 0; f < complication_arr[k] * 2 - 1; f++)
				//{
					//fs_print(&example_array[f]);
				//}


				compute_signal = compute_example(example_array, fs_stack, &fs_result, complication_arr[k] * 2 - 1, 3 - mask[k]);

				//printf("COMPUTE SIGNAL: %i\n", compute_signal);
				//printf("RESULT:\n");
				//fs_print(&fs_result);


				difficulty = fs_result.denominator_f * fs_result.integrity_f;

				//usleep(2000);

				if (compute_signal == TRUE_D && (difficulty > limit_u[k] || difficulty < limit_l[k]))
				{
					compute_signal = FALSE_D;
					//printf("BOUNDING [%li, %li]\n", limit_l[k], limit_u[k]);
					continue;
				}


				mss_p_1 = format(example_array, mss_stack, (complication_arr[k] * 2 - 1),
									cd[k], if_colon_notation);
				//puts("=======================");
			}

//================                 END                     ===================

			strcpy(mss_result_example.ms_s_characters_arr, mss_p_1->ms_s_characters_arr);
			frs_to_mss(&fs_result, &mss_result, 0);
			parse_res(mss_result.ms_s_characters_arr);

			m = 0;
			parse_all(mss_result_example.ms_s_characters_arr, 0);


    		fprintf(f3, "%c)", 'a' + k);
    		fprintf(f4, "%c)", 'a' + k);


			if ((k % 2) == 1) {
				if (if_last_1 == 1){
					fprintf(f1, "\\multicolumn{2}{l}{{\\large %c) ", 'a' + k);
					fprintf(f1, "$%s = %s$ }\\par }", mss_result_example.ms_s_characters_arr, mss_result.ms_s_characters_arr);
					fprintf(f1, "\\\\\n");
					if_last_1 = 0;
				}
				else {
					fprintf(f1, "{\\large %c) ", 'a' + k);
					fprintf(f1, "$%s = %s$ }\\par ", mss_result_example.ms_s_characters_arr, mss_result.ms_s_characters_arr);
					fprintf(f1, " \\\\\n");
				}
			}
			else
			{
				if ((complication_arr[k] + complication_arr[k + 1] > 7) || (k == how_many_examples - 1)){
					fprintf(f1, "\\multicolumn{2}{l}{{\\large %c) ", 'a' + k);
					fprintf(f1, "$%s = %s$ }\\par }", mss_result_example.ms_s_characters_arr, mss_result.ms_s_characters_arr);
					fprintf(f1, "\\\\\n");
					if_last_1 = 1;
				}
				else {
					fprintf(f1, "{\\large %c) ", 'a' + k);
					fprintf(f1, "$%s = %s$ }\\par ", mss_result_example.ms_s_characters_arr, mss_result.ms_s_characters_arr);
					fprintf(f1, " & \n");
				}
			}


			if ((k % 2) == 1) {
				if (if_last_1 == 1){
					fprintf(f2, "\\multicolumn{2}{l}{{\\large %c) ", 'a' + k);
					fprintf(f2, "$%s = $ }\\par }", mss_result_example.ms_s_characters_arr);
					fprintf(f2, "\\\\\n");
					if_last_2 = 0;
				}
				else {
					fprintf(f2, "{\\large %c) ", 'a' + k);
					fprintf(f2, "$%s = $ }\\par ", mss_result_example.ms_s_characters_arr);
					fprintf(f2, " \\\\\n");
				}
			}
			else
			{
				if ((complication_arr[k] + complication_arr[k + 1] > 7) || (k == how_many_examples - 1)){
					fprintf(f2, "\\multicolumn{2}{l}{{\\large %c) ", 'a' + k);
					fprintf(f2, "$%s = $ }\\par }", mss_result_example.ms_s_characters_arr);
					fprintf(f2, "\\\\\n");
					if_last_2 = 1;
				}
				else {
					fprintf(f2, "{\\large %c) ", 'a' + k);
					fprintf(f2, "$%s = $ }\\par ", mss_result_example.ms_s_characters_arr);
					fprintf(f2, " & \n");
				}
			}



			fprintf(f3, " $%s$, ", mss_result.ms_s_characters_arr);

			fprintf(f4, " \\mbox{$%s$} = $%s$, ", mss_result_example.ms_s_characters_arr, mss_result.ms_s_characters_arr);

			mybar_data->pbar_setvalue((int) (100 * h)/sheets );
		}/*To jest koniec petli dla przykladow*/

//============================================================================  
//================                 END                     ===================
//============================================================================

		fprintf(f1, "\\end{tabularx}\n}\\newline\n\\vspace{8mm}\n\n");
		fprintf(f2, "\\end{tabularx}\n}\\newline\n\\vspace{8mm}\n\n");
		fprintf(f3, "\\\\\\\\\n");
		fprintf(f4, "}\\vspace{4mm}\\\\\n");

	}/*To jest koniec petli glownej - dla kartkowek*/

//============================================================================
//============================================================================
//================                 END                     ===================
//============================================================================
//============================================================================

	fprintf(f1,"%s","\\end{document}");
	fprintf(f2,"%s","\\end{document}");
	fprintf(f3,"%s","\\end{document}");
	fprintf(f4,"%s","\\end{document}");

	fclose(f1);
	fclose(f2);
	fclose(f3);
	fclose(f4);

	return 0;
}

void print_info(FILE *h, long int how_many, long int * d_a, long int * i_a,
				long int * ll_a, long int * lu_a, char * name, int pi_info[]){
	char reg[MAX_LENGTH];
	int i;

	fprintf(h,"IDENTYFIKATOR: %s \\newline\n", name);
	fprintf(h,"Przyk\\l{}ad - maksymalny mianownik, maksymalna liczba ca\\l{}o\\'sci, przedzia\\l{} iloczynu ca\\l{}o\\'sci i mianownika wyniku:\\newline\n");

	for(i = 0; i < how_many; ++i){
		fprintf(h,"\n %c", 'a' + i);
		fprintf(h,") - ");
		my_itoa(reg, *d_a);
		fprintf(h,"%s, ", reg);

		my_itoa(reg, *i_a);
		fprintf(h,"%s, ", reg);

		my_itoa(reg, *ll_a);
		fprintf(h,"[%s, ", reg);

		my_itoa(reg, *lu_a);
		fprintf(h,"%s] ", reg);

		if (pi_info[i] & 1) fprintf(h, "Tylko dodatnie ");
		if (pi_info[i] & 2) fprintf(h, "Tylko ca\\l{}kowite");
		fprintf(h,"%s","\\newline");

		++d_a;
		++i_a;
		++ll_a;
		++lu_a;
	}
	fprintf(h,"%s","\n\\vspace{7mm}\n\n");

	return;
}

void change_id(char *a){
	char help_arr[MAX_LENGTH];
	unsigned int i,j=0;
   
	strcpy(help_arr,a);
	for(i = 0; i < strlen(a); ++i){
		if(a[i] != '_'){
			help_arr[j] = a[i];
			j++;
		}
		else{
			help_arr[j] = '\\';
			j++;
			help_arr[j] = '_';
			j++;
		}
	}
	help_arr[j] = '\0';
	strcpy(a, help_arr);

	return;
}

