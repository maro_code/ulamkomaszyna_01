#ifndef FRACTIONS_LIB
#define FRACTIONS_LIB

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include "bar.h"

//how long can be my_string_s structure
#define MAX_LENGTH 2000
//how many elements can have the longst example
#define MAX_COMPLICATION 30
//special number - it is approximation of square root of max_long_int
#define MAX_NUMBER 146542
#define TRUE_D 1
#define FALSE_D 0

#define COMPUTE_ERROR 4

#define P_I_MASK 0
//  00
#define NNP_I_MASK 1
//  10
#define P_NNI_MASK 2
//  01
#define NNP_NNI_MASK 3
//  11

#define P_I 3
#define NP_I 2
#define P_NI 1
#define NP_NI 0

#define OK_D 5

struct fraction_s{
	char sign_f;
	long int integrity_f;
	long int numerator_f;
	long int denominator_f;
};

struct my_string_s{
	char ms_s_characters_arr[MAX_LENGTH];
};

//arithmetical
long int gcd(long int x,long int y);
long int lcm(long int x,long int y);
long int my_abs(long int a);
long int decimal_parameter(long int a, long int b);

//generating example
int random_fraction(struct fraction_s *fr_s_p, long int max_d,
					long int max_i,	int plus_only, int ints_only);

int random_operator(struct fraction_s *fr_s_p);

int random_example(struct fraction_s example_elements[],
					int all_example_length, long int denominator,
					long int integrity, int plus_only, int ints_only);

//fraction operations
int compute_example(struct fraction_s *src_arr, struct fraction_s *stack_p,
					struct fraction_s *res_p, int all_elements, int p_i_mask);
int fs_add(struct fraction_s *fr_p_1, struct fraction_s *fr_p_2);
int fs_subtract(struct fraction_s *fr_p_1, struct fraction_s *fr_p_2);
int fs_multiply(struct fraction_s *fr_p_1, struct fraction_s *fr_p_2);
int fs_divide(struct fraction_s *fr_p_1, struct fraction_s *fr_p_2);
void fs_simplify(struct fraction_s *fr_p);
void fs_print(struct fraction_s *fr_p);
int fs_check(struct fraction_s *fr_p);

//text operations
void my_itoa(char *w,long int li_argument);
void frs_to_mss(struct fraction_s *ulam, struct my_string_s *n, long int x);
struct my_string_s *format(struct fraction_s *fr_p, struct my_string_s *stack_p,
							int all_elements, long int dec_not, int if_colon);
void ms_add(struct my_string_s *x, struct my_string_s *y, int if_paren);
void ms_subtract(struct my_string_s *x, struct my_string_s *y, int if_paren);
void ms_multiply(struct my_string_s *x, struct my_string_s *y, int if_paren);
void ms_divide(struct my_string_s *x, struct my_string_s *y, int if_paren, int if_colon);

//"parsers"
void cut_spaces(char *a, int begin_spaces);
void parse_1(char *a);
void parse_2(char *a);
void parse_3(char *a);
void parse_4(char *a);
void parse_res(char *a);
void parse_all(char *a, int if_dot_rem);

//high_level functions
int execute_work(std::string identyfikator, std::string main_path, MyBar *mybar_data);
void print_info(FILE *h, long int how_many, long int * d_a, long int * i_a,
				long int * ll_a, long int * lu_a, char * name, int pi_info[]);
void change_id(char *a);

#endif
