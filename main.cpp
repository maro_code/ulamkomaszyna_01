#include <QApplication>
#include <QWidget>
#include <QTranslator>

#include "frame.h"
#include "bar.h"

class MyWidget : public QWidget
{
public:
  MyWidget(QWidget *parent = 0);
};

//constructor-It constructs widget with an top-level layout only

MyWidget::MyWidget(QWidget *parent)
    : QWidget(parent)
{
  int i;

  QVBoxLayout *layout = new QVBoxLayout(this);
  this->resize(900, 200);
  int examples_c = 6; //CAUTION!! Here is 'constant'

  MyFrame *tab[examples_c];

  for(i = 0; i < examples_c; i++){
  tab[i] = new MyFrame(layout);
  }

  MyBar * process_bar = new MyBar(layout, tab, examples_c);

}


int main(int argc, char *argv[])
{
  QApplication app(argc, argv);
   QTranslator translator;
   translator.load("ulp_pl");
   app.installTranslator(&translator);
   
   MyWidget widget;
  widget.show();
  return app.exec();
}


