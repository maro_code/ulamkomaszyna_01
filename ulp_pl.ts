<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS><TS version="1.1" language="pl">
<defaultcodec></defaultcodec>
<context>
    <name>MyBar</name>
    <message>
        <location filename="bar.cpp" line="72"/>
        <source>Start</source>
        <translation>Start</translation>
    </message>
    <message>
        <location filename="bar.cpp" line="76"/>
        <source>Identifier:</source>
        <translation>Identyfikator:</translation>
    </message>
    <message>
        <location filename="bar.cpp" line="82"/>
        <source>Executed:</source>
        <translation>Wykonane:</translation>
    </message>
    <message>
        <location filename="bar.cpp" line="90"/>
        <source>License</source>
        <translation>Licencja</translation>
    </message>
    <message>
        <location filename="bar.cpp" line="94"/>
        <source>Try number:</source>
        <translation>Liczba prób:</translation>
    </message>
    <message>
        <location filename="bar.cpp" line="166"/>
        <source>No identifier!</source>
        <translation>Brak identyfikatora!</translation>
    </message>
    <message>
        <location filename="bar.cpp" line="166"/>
        <source>Please, give the identifier.</source>
        <translation>Podaj identyfikator.</translation>
    </message>
    <message>
        <location filename="bar.cpp" line="172"/>
        <source>Wrong identifier!</source>
        <translation>Nieodpowiedni identyfikator!</translation>
    </message>
    <message>
        <location filename="bar.cpp" line="172"/>
        <source>Identifier can contain only alphanumerics and - and _ characters.</source>
        <translation>Identyfikator może składać się wyłącznie z liter (bez polskich znaków),
cyfr, oraz znaków - i _.</translation>
    </message>
    <message>
        <location filename="bar.cpp" line="179"/>
        <source>Choose dirctory.</source>
        <translation>Wybierz katalog.</translation>
    </message>
    <message>
        <location filename="bar.cpp" line="190"/>
        <source>Warning</source>
        <translation>Uwaga</translation>
    </message>
    <message>
        <location filename="bar.cpp" line="190"/>
        <source>Files exist!
If You really want to overwrite - click &quot;Overwrite&quot;.
If don&apos;t - click &quot;Cancel&quot;.</source>
        <translation>Pliki istnieją!
Jeśli chcesz je nadpisać - kliknij &quot;Nadpisz&quot;.
Jeśli nie - kliknij &quot;Cancel&quot;.</translation>
    </message>
    <message>
        <location filename="bar.cpp" line="192"/>
        <source>Overwrite</source>
        <translation>Nadpisz</translation>
    </message>
    <message>
        <location filename="bar.cpp" line="210"/>
        <source>Error!</source>
        <translation>Błąd!</translation>
    </message>
    <message>
        <location filename="bar.cpp" line="210"/>
        <source>This approach failed.</source>
        <translation>Ta próba się nie powiodła.</translation>
    </message>
    <message>
        <location filename="bar.cpp" line="216"/>
        <source>No examples!</source>
        <translation>Brak przykładów!</translation>
    </message>
    <message>
        <location filename="bar.cpp" line="216"/>
        <source>You must check some examples.</source>
        <translation>Zaznacz jakiś przykład.</translation>
    </message>
    <message>
        <location filename="bar.cpp" line="222"/>
        <source>Error.</source>
        <translation>Błąd.</translation>
    </message>
    <message>
        <location filename="bar.cpp" line="222"/>
        <source>Such directory doesn&apos;t exist,
or You don&apos;t have permission to write to it.</source>
        <translation>Taki katalog nie istnieje, lub nie masz uprawnień do zapisu do niego.</translation>
    </message>
</context>
</TS>
