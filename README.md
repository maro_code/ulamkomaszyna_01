Install qt4 tools and latex:

$ apt-get install build-essential

$ apt-get install texlive texlive-lang-polish texlive-extra-utils

$ apt-get install libqt4-dev qt4-qmake


and then:

$ qmake

$ make

For compiling output use e.g. pdflatex like this

ID="put_id_here" ; pdflatex "$ID"_a.tex ; pdflatex "$ID"_b.tex ; pdflatex "$ID"_c.tex ; pdflatex "$ID"_d.tex