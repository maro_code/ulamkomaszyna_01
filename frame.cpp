#include <QApplication>
#include <QPushButton>
#include <QWidget>
#include <QObject>
#include <QFrame>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QGridLayout>
#include <QDial>
#include <QLCDNumber>
#include <QCheckBox>
#include <QLabel>
#include <iostream>

#include "frame.h"
#include "fractions.h"

MyFrame::MyFrame(QVBoxLayout * l, QWidget *parent)
  : QWidget(parent)
{

	QFrame *frame1 = new QFrame(parent);
	frame1 -> setFrameStyle(QFrame::Panel | QFrame::Raised);
	frame1 -> setLineWidth(1);
	l->addWidget(frame1);

	QHBoxLayout *layout1 = new QHBoxLayout(frame1);

	//Says whether this example will appear

	QCheckBox * example_check = new QCheckBox();
	layout1->addWidget(example_check);
	if_checked = 0;
	connect(example_check, SIGNAL(stateChanged(int)), this, SIGNAL(if_checked_signal(int)));
	connect(this, SIGNAL(if_checked_signal(int)), this, SLOT(set_if_checked(int)));
  

	//Displays number of figures in example

	QLCDNumber * Disp_Compl = new QLCDNumber(1);
	Disp_Compl->setSegmentStyle(QLCDNumber::Filled);
	Disp_Compl->display(3);
	Complication = 3;
	layout1->addWidget(Disp_Compl);

	QDial * Dial_Compl = new QDial();
	Dial_Compl->setMinimum(2);
	Dial_Compl->setMaximum(9);
	Dial_Compl->setValue(3);
	layout1->addWidget(Dial_Compl);

	connect(Dial_Compl, SIGNAL(valueChanged(int)), Disp_Compl, SLOT(display(int)));
	connect(Dial_Compl, SIGNAL(valueChanged(int)), this, SIGNAL(Compl_Changed(int)));
	connect(this, SIGNAL(Compl_Changed(int)), this, SLOT(set_Compl(int)));

	//Displays maximum denominator in example

	QLCDNumber * Disp_Denom = new QLCDNumber(2);
	Disp_Denom->setSegmentStyle(QLCDNumber::Filled);

	Disp_Denom->display(20);
	Max_Denominator = 20;
	layout1->addWidget(Disp_Denom);

	QDial * Dial_Denom = new QDial();
	Dial_Denom->setValue(20);
	layout1->addWidget(Dial_Denom);

	Dial_Denom->setMinimum(1);
	Dial_Denom->setMaximum(50);

	connect(Dial_Denom, SIGNAL(valueChanged(int)), Disp_Denom, SLOT(display(int)));
	connect(Dial_Denom, SIGNAL(valueChanged(int)), this, SIGNAL(Denom_Changed(int)));
	connect(this, SIGNAL(Denom_Changed(int)), this, SLOT(set_Denom(int)));


	//Displays maximum integrity in example

	QLCDNumber * Disp_Integr = new QLCDNumber(2);
	Disp_Integr->setSegmentStyle(QLCDNumber::Filled);
	Disp_Integr->display(4);
	Max_Integrity = 4;
	layout1->addWidget(Disp_Integr);

	QDial * Dial_Integr = new QDial();
	Dial_Integr->setValue(4);
	Dial_Integr->setMinimum(0);
	Dial_Integr->setMaximum(50);
	layout1->addWidget(Dial_Integr);

	connect(Dial_Integr, SIGNAL(valueChanged(int)), Disp_Integr, SLOT(display(int)));
	connect(Dial_Integr, SIGNAL(valueChanged(int)), this, SIGNAL(Integr_Changed(int)));
	connect(this, SIGNAL(Integr_Changed(int)), this, SLOT(set_Integr(int)));

	//Setting range of result's coefficient
	QFrame * res_frame = new QFrame;
	//res_frame->resize(200,100);
	layout1->addWidget(res_frame);

	QGridLayout * result_slider_layout = new QGridLayout(res_frame);

	Res_Upper = 50;
	Res_Lower = 10;

	QLCDNumber * lcd_lower = new QLCDNumber(3);
	lcd_lower->setSegmentStyle(QLCDNumber::Filled);
	lcd_lower->display(10);
	result_slider_layout->addWidget(lcd_lower,0,0);

	QLCDNumber * lcd_upper = new QLCDNumber(3);
	lcd_upper->setSegmentStyle(QLCDNumber::Filled);
	lcd_upper->display(50);
	result_slider_layout->addWidget(lcd_upper,1,0);

	QSlider * sli_lower = new QSlider(Qt::Horizontal);
	sli_lower->setRange(2,300);
	sli_lower->setValue(10);
	result_slider_layout->addWidget(sli_lower,0,1);

	QSlider * sli_upper = new QSlider(Qt::Horizontal);
	sli_upper->setRange(2,300);
	sli_upper->setValue(50);
	result_slider_layout->addWidget(sli_upper,1,1);

//   QPushButton * t_button = new QPushButton("Term");
//   layout1->addWidget(t_button);
//   connect(t_button, SIGNAL(clicked()), this, SLOT(send_to_terminal()));

	connect(sli_upper, SIGNAL(valueChanged(int)), lcd_upper, SLOT(display(int)));
	connect(sli_lower, SIGNAL(valueChanged(int)), lcd_lower, SLOT(display(int)));

	connect(sli_upper, SIGNAL(valueChanged(int)), this, SIGNAL(res_upper_changed(int)));
	connect(this, SIGNAL(res_upper_changed(int)), this, SLOT(set_res_upper(int)));

	connect(sli_lower, SIGNAL(valueChanged(int)), this, SIGNAL(res_lower_changed(int)));
	connect(this, SIGNAL(res_lower_changed(int)), this, SLOT(set_res_lower(int)));

	//here we're adding checkboxes to control "positive" & "int"
	//for each example
	if_po = 0;
	if_io = 0;

 	QLabel * po_label = new QLabel(tr("P_only:"));
	result_slider_layout->addWidget(po_label,0,2);

	QCheckBox * po_check_box = new QCheckBox;
	result_slider_layout->addWidget(po_check_box,0,3);

	connect(po_check_box, SIGNAL(stateChanged(int)), this, SIGNAL(po_signal(int)));
	connect(this, SIGNAL(po_signal(int)), this, SLOT(set_po(int)));

 	QLabel * io_label = new QLabel(tr("I_only:"));
	result_slider_layout->addWidget(io_label,1,2);

	QCheckBox * io_check_box = new QCheckBox;
	result_slider_layout->addWidget(io_check_box,1,3);

	connect(io_check_box, SIGNAL(stateChanged(int)), this, SIGNAL(io_signal(int)));
	connect(this, SIGNAL(io_signal(int)), this, SLOT(set_io(int)));


	QCheckBox * decimal_check_box = new QCheckBox;
	decimal_check_box->setTristate(true);
	layout1->addWidget(decimal_check_box);
	decimal = 0;
	connect(decimal_check_box, SIGNAL(stateChanged(int)), this, SIGNAL(decimal_checked_signal(int)));
	connect(this, SIGNAL(decimal_checked_signal(int)), this, SLOT(set_decimal_checked(int)));

}

//These functions return parameters

int MyFrame::f_Compl() const
{
	return Complication;
}

int MyFrame::f_Denom() const
{
	return Max_Denominator;
}

int MyFrame::f_Integr() const
{
	return Max_Integrity;
}

int MyFrame::f_res_upper() const
{
	return Res_Upper;
}

int MyFrame::f_res_lower() const
{
	return Res_Lower;
}

int MyFrame::f_decimal_checked() const
{
	return decimal;
}

int MyFrame::f_if_checked() const
{
	return if_checked;
}

int MyFrame::f_if_po() const
{
	return if_po;
}

int MyFrame::f_if_io() const
{
	return if_io;
}

//These functions set parameters - SLOTS

void MyFrame::set_Compl(int value)
{
	Complication = value;
	return;
}

void MyFrame::set_Denom(int value)
{
	Max_Denominator = value;
	return;
}

void MyFrame::set_Integr(int value)
{
	Max_Integrity = value;
	return;
}

void MyFrame::set_if_checked(int value)
{
	if_checked = value;
	return;
}

void MyFrame::set_decimal_checked(int value)
{
	decimal = value;
	return;
}

void MyFrame::set_res_upper(int value)
{
	Res_Upper = value;
	return;
}

void MyFrame::set_res_lower(int value)
{
	Res_Lower = value;
	return;
}

void MyFrame::set_po(int value)
{
	if_po = value;
	return;
}

void MyFrame::set_io(int value)
{
	if_io = value;
	return;
}

void MyFrame::send_to_terminal()
{
	std::cout << "Complication: " << Complication << std::endl
		<< "Max_Denominator: " << Max_Denominator << std::endl
		<< "Max_Integrity: " << Max_Integrity << std::endl
		<< "if_checked: " << if_checked << std::endl
		<< "Res_Lower: " << Res_Lower << std::endl
		<< "Res_Upper: " << Res_Upper << std::endl
		<< "decimal: " << decimal << std::endl
		<< "if_po: " << if_po << std::endl
		<< "if_io: " << if_io << std::endl
		<< std::endl;
	return;
}
