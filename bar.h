#ifndef MYBAR_H
#define MYBAR_H

#include <QApplication>
#include <QPushButton>
#include <QWidget>
#include <QObject>
#include <QString>
#include <QFrame>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QProgressBar>
#include <QPushButton>

#include <iostream>
#include <sstream>

#include "frame.h"

class MyBar : public QWidget
{
	Q_OBJECT

	public:
		MyBar(QVBoxLayout *l, MyFrame *tab[], int exam_c, QWidget *parent = 0);
		int get_how_many();
		int get_examples_counter();
		int get_try_bar();
		int check_id(QString x);
		void pbar_setvalue(int v);
		void pbar_reset();
		void try_over();
		void no_examples();
		void dir_error();
		MyFrame *get_example_pointer(int c);

	public slots:
		void throw_to_terminal();
		void process_dialog();
		void set_how_many(int newValue);
		void Show_License();
		void try_bar_set(int value);


	signals:
		void How_Many_Changed(int newValue);
		void try_bar_changed(int newValue);
		void positive_only_changed(int newValue);
		void ints_only_changed(int newValue);
		void colon_notation_changed(int newValue);

	private:
		MyFrame *examples[10];
		int examples_counter, how_many;
		//'how many' says how many sheets will be produced
		QLineEdit *identifier_lp;
		QString dir, id;
		QProgressBar *progress_bar;
		int licz_try_bar;
};

#endif
